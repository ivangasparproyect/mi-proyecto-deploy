import React, { Component } from 'react'
import { Inputs, Button, Row, Col } from "adminlte-2-react";
import { Redirect } from "react-router-dom";
const {Text}= Inputs;

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state={    
            user: {
                username: "",
                password: "",
            },
            alert: {},
            isLoad: false,
            redirectToReferrer: false,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(event){
     const user = this.state.user
     if(!user.username||!user.password){
         alert ('!Debe ingresar las credenciales');
         return;
     }
     if(user.username==="ivan"&&user.password==="ivan123"){
         this.setState({isLoad:true})
         this.setState({ redirectToReferrer: true });
         sessionStorage.setItem('login',true)
     }else{
         alert('usuario o password incorrecto ');
     }
     event.preventDefault();
    }
    
    render() {
        if (this.state.redirectToReferrer){
            return(<Redirect to={'/hello-world2'}/>);
        }
        const handleChange=(e)=>{
           let name = e.target.name;
           let value = e.target.value;
           let user = {...this.state.user};
           if (name==='username'){
               user.username=value
           }
           if (name==='password'){
               user.password=value;
           }
           this.setState({user:user})

        }
        const ButtonLogin = this.state.isLoad ? <Button
            pullRight
            type={"primary"}
            text={"Cargando ..."}
            disabled
        /> : <Button
                pullRight
                type={'success'}
                text={"Ingresar"}
                onClick={this.handleSubmit}
            />;
        return (<Row className={"hold-transition login-page"}>
        <Col md={12}>
            <div className="login-box">
                <div className="login-logo">
                    <a href="/"><b>System Ivan</b></a>
                </div>
                <div className="login-box-body">
                    <p className="login-box-msg">Inicia sesión para comenzar</p>
                    <div className={"msg-error text-center"}>
                        {this.state.alert.message}
                    </div>
                    <br />
                    <form>
                        <Text
                            name={"username"}
                            labelPosition={"none"}
                            placeholder={"Usuario"}
                            value={this.state.user.username}
                            onChange={handleChange}
                            iconLeft={"fa-user"}
                        />
                        <Text
                            name={"password"}
                            labelPosition={"none"}
                            placeholder={"Contraseña"}
                            inputType={"password"}
                            value={this.state.user.password}
                            onChange={handleChange}
                            iconLeft={"fa-lock"}
                        />
                        <Row>
                            <Col md={8} />
                            <Col md={4}>
                                {ButtonLogin}
                            </Col>
                        </Row>
                    </form>
                </div>
            </div>
        </Col>
    </Row >);
    }
}
