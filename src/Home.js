import React, { Component } from 'react';
import AdminLTE, { Sidebar, Content, Row, Col, Box, Button, Navbar } from 'adminlte-2-react';
import Login from './Login'
import Useritem from './components/Useritem'

const { Item } = Sidebar;

class HelloWorld extends Component {
  state = {}

  render() {
    return (<Content title="hola a todos" subTitle="Este es el sistema de ivan" browserTitle="system Ivan" >
      <Row>
        <Col xs={6}>
          <Box title="My first box" type="primary" closable collapsable footer={<Button type="danger" onClick={()=>alert("este es un boton")} text="Danger Button" />}>
            Hello World
          </Box>
        </Col>
        <Col xs={6}>
          <Box title="Another box">
            Content goes here
          </Box>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <Box title="Primera Plantilla" type="primary" closable collapsable footer={<Button type="danger" onClick={()=>alert("este es un boton")} text="Danger Button" />}>
            Esta es la primera plantilla 
            estoy probando react con deploy a heroku jajajajajaXDXD
            soy Ivan
          </Box>
        </Col>
        <Col xs={6}>
          <Box title="Nota De Texto">
            aca va una nota de texto 
            pueden poner lo que quieran 
            espera no se puede editar .... Xd

          </Box>
        </Col>
      </Row>
    </Content>);
  }
}
class Home extends Component {
  state = {}

  render() {
    return (<Content title="hola a todos" subTitle="Este es el sistema de ivan" browserTitle="system Ivan" >
      <Row>
        <Col xs={6}>
          <Box title="My first box" type="primary" closable collapsable footer={<Button type="danger" onClick={()=>alert("este es el home")} text="Danger Button" />}>
            este es mi primer App
          </Box>
        </Col>
        <Col xs={6}>
          <Box title="Another box">
            Content goes here
          </Box>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <Box title="Primera Plantilla" type="primary" closable collapsable footer={<Button type="danger" onClick={()=>alert("este es un boton")} text="Danger Button" />}>
            Esta es la primera plantilla 
            estoy probando react con deploy a heroku jajajajajaXDXD
            soy Ivan
          </Box>
        </Col>
        <Col xs={6}>
          <Box title="Nota De Texto">
            aca va una nota de texto 
            pueden poner lo que quieran 
            espera no se puede editar .... Xd

          </Box>
        </Col>
      </Row>
    </Content>);
  }
}
class HelloWorld2 extends Component {
  state = {}

  render() {
    return (<Content title="Hello World" subTitle="Getting started with adminlte-2-react" browserTitle="Hello World">
      <Row>
        <Col xs={6}>
          <Box title="My first box" type="primary" closable collapsable footer={<Button type="danger" onClick={()=>alert("este es un boton")} text="Danger Button" />}>
            Hello World
          </Box>
        </Col>
        <Col xs={6}>
          <Box title="Another box">
            Content goes here
          </Box>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <Box title="Primera Plantilla" type="primary" closable collapsable footer={<Button type="danger" onClick={()=>alert("este es un boton")} text="Danger Button" />}>
            Esta es la primera plantilla 
            estoy probando react con deploy a heroku jajajajajaXDXD
            soy Ivan
          </Box>
        </Col>
        <Col xs={6}>
          <Box title="Nota De Texto">
            aca va una nota de texto 
            pueden poner lo que quieran 
            espera no se puede editar .... Xd

          </Box>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <Box title="My first box" type="primary" closable collapsable footer={<Button type="danger" onClick={()=>alert("este es un boton")} text="Danger Button" />}>
            Hello World
          </Box>
        </Col>
        <Col xs={6}>
          <Box title="Another box">
            Content goes here
          </Box>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <Box title="Primera Plantilla" type="primary" closable collapsable footer={<Button type="danger" onClick={()=>alert("este es un boton")} text="Danger Button" />}>
            Esta es la primera plantilla 
            estoy probando react con deploy a heroku jajajajajaXDXD
            soy Ivan
          </Box>
        </Col>
        <Col xs={6}>
          <Box title="Nota De Texto">
            aca va una nota de texto 
            pueden poner lo que quieran 
            espera no se puede editar .... Xd

          </Box>
        </Col>
      </Row>
    </Content>);
  }
}

class App extends Component {

  sidebar = [
    <Item key="hello" text="item1" to="/hello-world" />,
    <Item key="hello" text="item2" to="/hello-world2" />
  ]
  navbar=[
    <Useritem
              username={'ivan'}
              profile={'Administrador'}
              email={'jl_libra_24@hotmail.com'}
              imageUrl={'https://3.bp.blogspot.com/-j4wuuvRqAI0/VxAvR8ZE6YI/AAAAAAAAERM/BwV0Td6xDbUL4ubouxeTqodVnikRHTZfACLcB/s1600/campeon.jpg'}
             />
  ]

  render() {
    if(sessionStorage.getItem('login')){
    return (
      <AdminLTE title={["Negocio", "Ivan"]} titleShort={["Ne", "In"]} theme="green" sidebar={this.sidebar} >
         <Navbar.Core>
             <Useritem
              username={'ivan'}
              profile={'Administrador'}
              email={'jl_libra_24@hotmail.com'}
              imageUrl={'https://3.bp.blogspot.com/-j4wuuvRqAI0/VxAvR8ZE6YI/AAAAAAAAERM/BwV0Td6xDbUL4ubouxeTqodVnikRHTZfACLcB/s1600/campeon.jpg'}
             />
        </Navbar.Core> 
        <Home path="/" exact/>
        <HelloWorld path="/hello-world" exact/>
        <HelloWorld2 path="/hello-world2" exact/>
      </AdminLTE>
    )}else{
      return(<Login/>)
    }
  }
}


export default App;